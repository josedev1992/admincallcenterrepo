﻿using System;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using System.Collections.Generic;

namespace AdminCallCenter.Security
{
    public class CustomAuthenticationModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.AuthenticateRequest += OnAuthenticateRequest;
        }

        public void OnAuthenticateRequest(object sender, EventArgs e)
        {
            string identityCookieName = "TPINVAPP";
            HttpApplication app = (HttpApplication)sender;
            HttpCookie identityCookie = app.Context.Request.Cookies[identityCookieName];

            if (identityCookie == null)
                return;

            FormsAuthenticationTicket identityTicket = (FormsAuthenticationTicket)null;
            try
            {
                identityTicket = FormsAuthentication.Decrypt(identityCookie.Value);
            }
            catch
            {
                app.Context.Request.Cookies.Remove(identityCookieName);
                return;
            }

            string name = "";

            HttpCookie authCookie = app.Context.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                try
                {
                    FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                    if (authTicket.Name != identityTicket.Name) return;
                    name = authTicket.Name;
                }
                catch
                {
                    app.Context.Request.Cookies.Remove(FormsAuthentication.FormsCookieName);
                    return;
                }
            }
            if (name != string.Empty)
            {
                var customIdentity = new CustomIdentity(name, identityTicket.UserData, "");
                var userPrincipal = new GenericPrincipal(customIdentity, new string[0]);
                app.Context.User = userPrincipal;
            }
            else
            {
                var customIdentity = new CustomIdentity(name, identityTicket.UserData);
                var userPrincipal = new GenericPrincipal(customIdentity, new string[0]);
                app.Context.User = userPrincipal;
            }
        }

        public void Dispose()
        {
        }
    }
}