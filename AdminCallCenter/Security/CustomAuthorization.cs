﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TPCO.AppInventory.Security
{
    //[AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    //public class CustomAuthorization : AuthorizeAttribute
    //{
    //    public int OperationKey { get; set; }

    //    protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
    //    {
    //        if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
    //        {
    //            filterContext.Result = new RedirectToRouteResult(
    //            new RouteValueDictionary(new { controller = "Account", action = "LogOn" })
    //            );
    //        }
    //        else
    //        {
    //            filterContext.Result = new RedirectToRouteResult(
    //            new RouteValueDictionary(new { controller = "Error", action = "Unauthorized" })
    //            );
    //        }
    //    }

    //    protected override bool AuthorizeCore(HttpContextBase httpContext)
    //    {
    //        bool isAuthorized = false;
    //        string currentMenuId = SecurityID.CurrentMenuId;
    //        List<Models.RelRoleMenu> MenusAuthByUser = new List<Models.RelRoleMenu>();
    //        if (httpContext.Session["USRAUTH"] != null)
    //        {
    //            MenusAuthByUser = (List<Models.RelRoleMenu>)httpContext.Session["USRAUTH"];
    //            List<Models.RelRoleMenu> menuAuth = MenusAuthByUser.Where(a => a.MenuId == currentMenuId).ToList();
    //            if (menuAuth.Count > 0)
    //            {
    //                foreach (Models.RelRoleMenu itmMnuAuth in menuAuth)
    //                {
    //                    switch (OperationKey)
    //                    {
    //                        case (int)Helpers.Enums.AuthorizationActions.List:
    //                            isAuthorized = true;
    //                            break;
    //                        case (int)Helpers.Enums.AuthorizationActions.Insert:
    //                            if (!isAuthorized)
    //                                isAuthorized = itmMnuAuth.allowNew == null ? false : (bool)itmMnuAuth.allowNew;
    //                            break;
    //                        case (int)Helpers.Enums.AuthorizationActions.Update:
    //                            if (!isAuthorized)
    //                                isAuthorized = itmMnuAuth.allowModify == null ? false : (bool)itmMnuAuth.allowModify;
    //                            break;
    //                        case (int)Helpers.Enums.AuthorizationActions.Delete:
    //                            if (!isAuthorized)
    //                                isAuthorized = itmMnuAuth.allowDelete == null ? false : (bool)itmMnuAuth.allowDelete;
    //                            break;
    //                        case (int)Helpers.Enums.AuthorizationActions.Special1:
    //                            if (!isAuthorized)
    //                                isAuthorized = itmMnuAuth.Special1 == null ? false : (bool)itmMnuAuth.Special1;
    //                            break;
    //                        case (int)Helpers.Enums.AuthorizationActions.Special2:
    //                            if (!isAuthorized)
    //                                isAuthorized = itmMnuAuth.Special2 == null ? false : (bool)itmMnuAuth.Special2;
    //                            break;
    //                        default:
    //                            isAuthorized = false;
    //                            break;
    //                    }
    //                }
    //            }
    //        }
    //        return isAuthorized;
    //    }
    //}
}