﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace AdminCallCenter.Security
{
    public class CustomAuthentication
    {
        public static void SetAuthCookies(string userName, string fullName)
        {
            FormsAuthenticationTicket identityTicket = new FormsAuthenticationTicket(1, userName, DateTime.Now, DateTime.Now.AddMinutes(30), true, fullName);
            string encryptedIdentityTicket = FormsAuthentication.Encrypt(identityTicket);
            var identityCookie = new HttpCookie("TPINVAPP", encryptedIdentityTicket);

            identityCookie.Expires = DateTime.Now.AddMinutes(30);
            HttpContext.Current.Response.Cookies.Add(identityCookie);

            FormsAuthentication.SetAuthCookie(userName, false);
        }
    }
}