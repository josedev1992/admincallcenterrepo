﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TPCO.AppInventory.Security
{
    public static class SecurityID
    {
        public static string CurrentMenuId
        {
            get
            {
                if (HttpContext.Current.Session["TagID"] != null)
                    return HttpContext.Current.Session["TagID"].ToString();
                else
                    return "";
            }
        }
    }
}