﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportMVC.Helpers
{
    public class Enums
    {
        public enum Action
        {
            List = 1,
            Insert = 2,
            Update = 3,
            Delete = 4,
            Special1 = 5,
            Special2 = 6
        }
    }
}