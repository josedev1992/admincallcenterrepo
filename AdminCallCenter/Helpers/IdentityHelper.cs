﻿using AdminCallCenter.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminCallCenter.Helpers
{
    public class IdentityHelper
    {
        public static CustomIdentity CurrentUser
        {
            get
            {
                if (HttpContext.Current.User.Identity is CustomIdentity)
                    return (CustomIdentity)HttpContext.Current.User.Identity;

                return CustomIdentity.EmptyIdentity;
            }
        }
    }
}