$(function ($) {
    setTimeout(function () {
        $('#content-wrapper > .row').css({
            opacity: 1
        });
    }, 200);

    $('#sidebar-nav .dropdown-toggle').on('click', function (e) {
        e.preventDefault();

        var $item = $(this).parent();

        if (!$item.hasClass('open')) {
            $item.parent().find('.open .submenu').slideUp('fast');
            $item.parent().find('.open').toggleClass('open');
        }

        $item.toggleClass('open');

        if ($item.hasClass('open')) {
            $item.children('.submenu').slideDown('fast');
        }
        else {
            $item.children('.submenu').slideUp('fast');
        }
    });

    $('body').on('mouseenter', '#page-wrapper.nav-small #sidebar-nav .dropdown-toggle', function (e) {
        var $sidebar = $(this).parents('#sidebar-nav');

        if ($(document).width() >= 992) {
            var $item = $(this).parent();

            $item.addClass('open');
            $item.children('.submenu').slideDown('fast');
        }
    });

    $('body').on('mouseleave', '#page-wrapper.nav-small #sidebar-nav > .nav-pills > li', function (e) {
        var $sidebar = $(this).parents('#sidebar-nav');

        if ($(document).width() >= 992) {
            var $item = $(this);

            if ($item.hasClass('open')) {
                $item.find('.open .submenu').slideUp('fast');
                $item.find('.open').removeClass('open');
                $item.children('.submenu').slideUp('fast');
            }

            $item.removeClass('open');
        }
    });

    $('#make-small-nav').click(function (e) {
        $('#page-wrapper').toggleClass('nav-small');
    });

    $(window).smartresize(function () {
        if ($(document).width() <= 991) {
            $('#page-wrapper').removeClass('nav-small');
        }
    });

    $('.mobile-search').click(function (e) {
        e.preventDefault();

        $('.mobile-search').addClass('active');
        $('.mobile-search form input.form-control').focus();
    });
    $(document).mouseup(function (e) {
        var container = $('.mobile-search');

        if (!container.is(e.target) // if the target of the click isn't the container...
			&& container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            container.removeClass('active');
        }
    });

    $('.fixed-leftmenu #col-left').nanoScroller({
        alwaysVisible: true,
        iOSNativeScrolling: false,
        preventPageScrolling: true,
        contentClass: 'col-left-nano-content'
    });

    // build all tooltips from data-attributes
    $("[data-toggle='tooltip']").each(function (index, el) {
        $(el).tooltip({
            placement: $(this).data("placement") || 'top'
        });
    });
});

$.fn.removeClassPrefix = function (prefix) {
    this.each(function (i, el) {
        var classes = el.className.split(" ").filter(function (c) {
            return c.lastIndexOf(prefix, 0) !== 0;
        });
        el.className = classes.join(" ");
    });
    return this;
};

(function ($, sr) {
    // debouncing function from John Hann
    // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
    var debounce = function (func, threshold, execAsap) {
        var timeout;

        return function debounced() {
            var obj = this, args = arguments;
            function delayed() {
                if (!execAsap)
                    func.apply(obj, args);
                timeout = null;
            };

            if (timeout)
                clearTimeout(timeout);
            else if (execAsap)
                func.apply(obj, args);

            timeout = setTimeout(delayed, threshold || 100);
        };
    }
    // smartresize 
    jQuery.fn[sr] = function (fn) { return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery, 'smartresize');

 $.fn.select2.defaults.set("theme", "bootstrap");
$.fn.select2.defaults.set('language--noResults', function () { return "Sin resultados."; });

function OpenMenu(mn) {
    $("#" + mn).parents('li').addClass('open');
    $("#" + mn).parents('ul').attr('style', 'display:block');
};

/**
 * A Javascript module to loadeding/refreshing options of a select2 list box using ajax based on selection of another select2 list box.
 *
 * @url : https://gist.github.com/ajaxray/187e7c9a00666a7ffff52a8a69b8bf31
 * @auther : Anis Uddin Ahmad <anis.programmer@gmail.com>
 *
 * Live demo - https://codepen.io/ajaxray/full/oBPbQe/
 * w: http://ajaxray.com | t: @ajaxray
 */
var Select2Cascade = (function (window, $) {

    function Select2Cascade(parent, child, url, options) {
        var afterActions = [];

        // Register functions to be called after cascading data loading done
        this.then = function (callback) {
            afterActions.push(callback);
            return this;
        };

        parent.select2(options).on("change", function (e) {

            child.prop("disabled", true);
            var _this = this;
            $.getJSON(url.replace(':parentId:', $(this).val()), function (items) {
                var newOptions = '<option value=""></option>';
                for (var i in items.data) {
                    newOptions += '<option value="' + items.data[i].Key + '">' + items.data[i].Value + '</option>';
                }

                child.select2('destroy').html(newOptions).prop("disabled", false)
                    .select2(options).trigger('change');

                afterActions.forEach(function (callback) {
                    callback(parent, child, items, options);
                });
            });
        });
    }

    return Select2Cascade;

})(window, $);


