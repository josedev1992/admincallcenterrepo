﻿$.extend(true, $.fn.dataTable.defaults, {
    dom: 'rtip',
    buttons: [{
        extend: 'excel', text: 'Exportar a Excel', action: function (e, dt, node, config) {
            exportExtension = 'Excel';
            $.fn.DataTable.ext.buttons.excelHtml5.action.call(this, e, dt, node, config);
        }
    },
              {
                  extend: 'csv', text: 'Exportar a CSV', action: function (e, dt, node, config) {
                      exportExtension = 'CSV';
                      $.fn.DataTable.ext.buttons.csvHtml5.action.call(this, e, dt, node, config);
                  }
              },
              {
                  extend: 'copy', text: 'Copiar', action: function (e, dt, node, config) {
                      exportExtension = 'Copy';
                      $.fn.DataTable.ext.buttons.copyHtml5.action.call(this, e, dt, node, config);
                  }
              }],

    language: {
        sProcessing: "Procesando...",
        sLengthMenu: "Mostrar _MENU_ registros",
        sZeroRecords: "No se encontraron resultados",
        sEmptyTable: "Ningún dato disponible en esta tabla",
        sInfo: "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        sInfoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
        sInfoFiltered: "(filtrado de un total de _MAX_ registros)",
        sInfoPostFix: "",
        sSearch: "Buscar:",
        sUrl: "",
        sInfoThousands: ",",
        sLoadingRecords: "Cargando...",
        oPaginate: {
            sFirst: "Primero",
            sLast: "Último",
            sNext: "Siguiente",
            sPrevious: "Anterior"
        },
        oAria: {
            sSortAscending: ": Activar para ordenar la columna de manera ascendente",
            sSortDescending: ": Activar para ordenar la columna de manera descendente"
        },
        buttons: {
            copyTitle: 'Copiado al portapapeles',
            copySuccess: {
                _: 'Copiados %d registros',
                1: 'Copiado 1 registro'
            }
        }
    }
})

$(function () {
    $('#btnExportCSV').click(function () {
        $.fn.dataTable.tables({ visible: true, api: true }).buttons('.buttons-csv').trigger();
    })
    $('#btnExportExcel').click(function () {
        $.fn.dataTable.tables({ visible: true, api: true }).buttons('.buttons-excel').trigger();
    })
    $('#btnExportClip').click(function () {
        $.fn.dataTable.tables({ visible: true, api: true }).buttons('.buttons-copy').trigger();
    })
    $('#txtsearch').on('keyup', function () {
        $.fn.dataTable.tables({ visible: true, api: true }).search(this.value).draw();
    });
});

window.simple_checkbox = function (data, type, full, meta) {
    var is_checked = data == true ? "checked" : "";
    return '<input type="checkbox" class="checkbox" disabled="disabled" ' +
        is_checked + ' />';
};
