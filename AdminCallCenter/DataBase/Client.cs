//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdminCallCenter.DataBase
{
    using System;
    using System.Collections.Generic;
    
    public partial class Client
    {
        public int Id_Client { get; set; }
        public string DocumentType { get; set; }
        public string Document { get; set; }
        public string Name { get; set; }
        public string MobilePhone { get; set; }
        public int City { get; set; }
        public System.DateTime Birthdate { get; set; }
    
        public virtual City City1 { get; set; }
        public virtual DocumentType DocumentType1 { get; set; }
    }
}
