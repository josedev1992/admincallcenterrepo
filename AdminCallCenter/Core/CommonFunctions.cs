﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Web;

namespace CasesMassUpdate.Core
{
    public class CommonFunctions
    {
        public static DataTable XlsCreateDataTable(string formato, string ruta, ref long result, ref string resultDescription)
        {
            resultDescription = "";
            result = -999;
            try
            {
                string _cadenaconexion = "";
                switch (formato.ToLower())
                {
                    case ".xlsx":
                        _cadenaconexion = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0;HDR=NO'", ruta);
                        //Excel 12.0 Xml
                        break;
                    case ".xls":
                        _cadenaconexion = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0;HDR=NO'", ruta);
                        //Excel 12.0 Xml
                        break;
                    //_cadenaconexion = String.Format("Provider=Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=Excel 8.0", ruta)
                }

                OleDbConnection _oledbConn = new OleDbConnection(_cadenaconexion);
                DataTable dt = new DataTable();

                _oledbConn.Open();
                OleDbCommand _cmd = new OleDbCommand("SELECT * FROM [BD$]", _oledbConn);
                OleDbDataAdapter _oleda = new OleDbDataAdapter();
                _oleda.SelectCommand = _cmd;

                _oleda.Fill(dt);

                _oleda.Dispose();
                _cmd.Dispose();

                _oledbConn.Close();
                _oledbConn.Dispose();

                result = 1;
                resultDescription = "";
                //System.IO.File.Delete(ruta);
                return dt;
            }
            catch (Exception ex)
            {
                System.IO.File.Delete(ruta);
                result = -999;
                resultDescription = ex.Message;
                return null;
            }
        }
        public static DataTable LoadCSVtoDataTable(string FilePath, char Separator, char TextQualifier, ref Int64 result, ref string resultDescription)
        {
            //Contenido de cada una de las líneas del archivo
            string Line = string.Empty;
            //Columnas que contiene cada línea
            List<string> Fields = new List<string>();
            DataTable DT = new DataTable();
            System.IO.FileStream FS = default(System.IO.FileStream);
            System.IO.StreamReader SR = default(System.IO.StreamReader);

            Int64 NLine = 0;
            try
            {
                DT = new DataTable();
                FS = new System.IO.FileStream(FilePath, System.IO.FileMode.Open);
                SR = new System.IO.StreamReader(FS, System.Text.Encoding.Default);
                Line = SR.ReadLine();
                NLine = NLine + 1;
                Fields = CSVSplit(Line, Separator, TextQualifier);
                for (int I = 0; I <= Fields.Count - 1; I++)
                {
                    DT.Columns.Add(Strings.Replace(Fields[I].Trim(), "\"", null));
                }
                Line = SR.ReadLine();
                NLine = NLine + 1;
                while ((Line != null))
                {
                    if (Line.Trim() != string.Empty)
                    {
                        Fields = CSVSplit(Line, Separator, TextQualifier);
                        if (Fields.Count > DT.Columns.Count)
                        {
                            throw new Exception("La línea contiene delimitadores dentro del texto, pero los delimitadores no están contenidos entre calificadores de texto.");
                        }
                        DataRow DR;
                        DR = DT.NewRow();
                        for (int I = 0; I <= Fields.Count - 1; I++)
                        {
                            DR[I] = Fields[I].Trim().Replace("\"", null);
                        }
                        DT.Rows.Add(DR);
                    }
                    Line = SR.ReadLine();
                    NLine = NLine + 1;
                }
                result = 1;
                return DT;
            }
            catch (Exception ex)
            {
                result = -999;
                resultDescription = ex.Message;
                return DT;
            }
            finally
            {
                if ((SR != null))
                    SR.Close();
                if ((FS != null))
                    FS.Close();
                if ((DT != null))
                    DT.Dispose();
            }
        }

        public static List<string> CSVSplit(string Input, char Delimiter, char TextQualifier)
        {
            List<string> Output = new List<string>();
            string Field = string.Empty;
            bool InsideQualifierFlag = false;
            //Recorremos caracter a caracter
            for (int I = 0; I <= Input.Length - 1; I++)
            {
                if (Field == Delimiter.ToString())
                    Field = string.Empty;
                if (Input[I] == TextQualifier)
                {
                    InsideQualifierFlag = !InsideQualifierFlag;
                }
                if (!InsideQualifierFlag)
                {
                    if (Input[I] == Delimiter)
                    {
                        Output.Add(ValidChars(Field.Replace(TextQualifier.ToString(), string.Empty)));
                        Field = string.Empty;
                    }
                }
                Field = Field + Input[I];
            }
            if (Field != string.Empty)
            {
                if (Field == Delimiter.ToString())
                {
                    Output.Add(ValidChars(Field.Replace(Delimiter.ToString(), string.Empty)));
                }
                else
                {
                    Output.Add(ValidChars(Field.Replace(TextQualifier.ToString(), string.Empty)));
                }
            }
            return Output;
        }

        public static string ValidChars(string Input)
        {
            if (Input != null)
            {
                Input = Input.Replace("'", string.Empty);
            }
            return Input;
        }
    }
}