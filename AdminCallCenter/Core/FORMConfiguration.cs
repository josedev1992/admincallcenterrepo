﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace formulario_contacto.Core
{
    public class FORMConfiguration
    {
        public class Tree
        {
            public string Id_TREE { get; set; }
            public string Description { get; set; }
        }
        public class Branch
        {
            public string id { get; set; }
            public string label { get; set; }
        }
        public class SaveCaseResponse
        {
            public string code { get; set; }
            public string description { get; set; }
        }
        
        public static bool GetTrees(ref List<Tree> TreeList)
        {
            bool blnR;
            string strSQL;
            long lngRows;

            DatabaseManager objConexion = new DatabaseManager();
            string response = "";
            SqlParameter[] parametros = null;
            try
            {
                blnR = objConexion.OpenDatabase(ConfigurationManager.AppSettings["SubsidiaryConectionString"]);
                if (blnR)
                {
                    strSQL = "CAMU_S_Trees";

                    lngRows = objConexion.QueryDatabase(strSQL, ref parametros);
                    objConexion.CloseDatabase();
                    if (lngRows > 0)
                    {
                        for (int i = 0; i <= lngRows - 1; i++)
                        {
                            Tree TT = new Tree();
                            TT.Id_TREE = objConexion.DataResults(i, "Id_TREE").ToString();
                            TT.Description = objConexion.DataResults(i, "Description").ToString();
                            TreeList.Add(TT);
                        }
                        return true;
                    }
                }
                return false;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public static bool FORM_S_RootBranchesByTree(string Id_TREE, ref List<Branch> Branches)
        {
            bool blnR;
            string strSQL;
            long lngRows;
            SqlParameter p1 = new SqlParameter();
            DatabaseManager objConexion = new DatabaseManager();
            string response = "";
            SqlParameter[] parametros = new SqlParameter[1];
            try
            {
                blnR = objConexion.OpenDatabase(ConfigurationManager.AppSettings["SubsidiaryConectionString"]);
                if (blnR)
                {
                    strSQL = "FORM_S_RootBranchesByTree";
                    p1.ParameterName = "Id_TREE";
                    p1.SqlDbType = SqlDbType.NVarChar;
                    p1.Value = Id_TREE;
                    parametros[0] = p1;


                    lngRows = objConexion.QueryDatabase(strSQL, ref parametros);
                    objConexion.CloseDatabase();
                    if (lngRows > 0)
                    {
                        for (int i = 0; i <= lngRows - 1; i++)
                        {
                            Branch bran = new Branch();
                            bran.id = objConexion.DataResults(i, "Id_BRAN").ToString();
                            bran.label = objConexion.DataResults(i, "Description").ToString();
                            Branches.Add(bran);
                        }
                        return true;
                    }
                }
                return false;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static bool FORM_S_BranchesByTree(string Father_ID_BRAN, ref List<Branch> Branches)
        {
            bool blnR;
            string strSQL;
            long lngRows;
            SqlParameter p1 = new SqlParameter();
            DatabaseManager objConexion = new DatabaseManager();
            string response = "";
            SqlParameter[] parametros = new SqlParameter[1];
            try
            {
                blnR = objConexion.OpenDatabase(ConfigurationManager.AppSettings["SubsidiaryConectionString"]);
                if (blnR)
                {
                    strSQL = "FORM_S_BranchesByTree";
                    p1.ParameterName = "Father_ID_BRAN";
                    p1.SqlDbType = SqlDbType.NVarChar;
                    p1.Value = Father_ID_BRAN;
                    parametros[0] = p1;


                    lngRows = objConexion.QueryDatabase(strSQL, ref parametros);
                    objConexion.CloseDatabase();
                    if (lngRows > 0)
                    {
                        for (int i = 0; i <= lngRows - 1; i++)
                        {
                            Branch bran = new Branch();
                            bran.id = objConexion.DataResults(i, "Id_BRAN").ToString();
                            bran.label = objConexion.DataResults(i, "Description").ToString();
                            Branches.Add(bran);
                        }
                        return true;
                    }
                }
                return false;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public static string ToExcelIndexCol(double n, ref string res)
        {
            string functionReturnValue = null;
            if ((n == 0))
            {
                //Console.Write("Z");
                functionReturnValue = "";
            }
            else
            {
                int r = Convert.ToInt32(n % 26);
                n = (n / 26);
                if (n < 1)
                {
                    n = 0;
                }
                else
                {
                    if (n >= 1 & n < 2)
                    {
                        n = 1;
                    }
                    else
                    {
                        if (n >= 2 & n < 3)
                        {
                            n = 2;
                        }
                        else
                        {
                            if (n >= 3 & n < 4)
                            {
                                n = 3;
                            }
                            else
                            {
                                n = Math.Round(n, 0);
                            }
                        }
                    }
                }
                if ((r == 0))
                {
                    ToExcelIndexCol((n - 1), ref res);
                }
                else
                {
                    ToExcelIndexCol(n, ref res);
                }
                if ((r == 0))
                {
                    res = (res + "Z");
                    if ((n == 1))
                    {
                        functionReturnValue = "";
                    }
                }
                switch ((r))
                {
                    case 1:
                        res = (res + "A");
                        break;
                    case 2:
                        res = (res + "B");
                        break;
                    case 3:
                        res = (res + "C");
                        break;
                    case 4:
                        res = (res + "D");
                        break;
                    case 5:
                        res = (res + "E");
                        break;
                    case 6:
                        res = (res + "F");
                        break;
                    case 7:
                        res = (res + "G");
                        break;
                    case 8:
                        res = (res + "H");
                        break;
                    case 9:
                        res = (res + "I");
                        break;
                    case 10:
                        res = (res + "J");
                        break;
                    case 11:
                        res = (res + "K");
                        break;
                    case 12:
                        res = (res + "L");
                        break;
                    case 13:
                        res = (res + "M");
                        break;
                    case 14:
                        res = (res + "N");
                        break;
                    case 15:
                        res = (res + "O");
                        break;
                    case 16:
                        res = (res + "P");
                        break;
                    case 17:
                        res = (res + "Q");
                        break;
                    case 18:
                        res = (res + "R");
                        break;
                    case 19:
                        res = (res + "S");
                        break;
                    case 20:
                        res = (res + "T");
                        break;
                    case 21:
                        res = (res + "U");
                        break;
                    case 22:
                        res = (res + "V");
                        break;
                    case 23:
                        res = (res + "W");
                        break;
                    case 24:
                        res = (res + "X");
                        break;
                    case 25:
                        res = (res + "Y");
                        break;
                    case 26:
                        res = (res + "Z");
                        break;
                }
            }
            functionReturnValue = res;
            return functionReturnValue;
        }
       
    }
}