﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Linq.Expressions;
using System.Globalization;
using TPCO.Helpers.TPClientInterface;
using TPCO.Helpers.TPClientInterface.TPClient;

namespace AdminCallCenter.Class
{
    public class DataAccess
    {
        #region Calls for Client

        public static Response getClients(ref List<DataModel> listHeader, ref List<DataModel> listData, string dsn)
        {
            Response resp = new Response();
            listHeader = new List<DataModel>();
            listData = new List<DataModel>();

            try
            {
                DatabaseManager objConexion = new DatabaseManager();
                SqlParameter[] parametros = new SqlParameter[0];

                string strSQL = "";

                bool bolR = false;
                long lngR = 0;
                long lngI = 0;
                int indice;

                bolR = objConexion.OpenDatabase(dsn);
                if (!bolR)
                {
                    resp.responseDescription = "NO CONNECTION BD";
                    resp.responseCode = -2;
                    return resp;
                }

                strSQL = "sel_Clients";

                lngR = objConexion.QueryDatabaseList(strSQL, ref parametros);
                //objConexion.CloseDatabase();

                if (lngR == -999)
                {
                    resp.responseDescription = objConexion.LastError;
                    resp.responseCode = -999;
                    return resp;
                }

                if (lngR >= 0)
                {
                    listHeader = objConexion.GetHeaderList();
                    listData = objConexion.GetDataList();
                    resp.responseDescription = "OK";
                    resp.responseCode = 1;
                }
                else
                {
                    resp.responseDescription = "NO SELECT CLIENTS -- ERROR: " + objConexion.LastError;
                    resp.responseCode = -1;

                }

                objConexion.CloseDatabase();
                return resp;
            }
            catch (Exception ex)
            {
                resp.responseDescription = "ERROR: " + ex.Message;
                resp.responseCode = -3;
                return resp;
            }
        }

        #endregion
    }
}