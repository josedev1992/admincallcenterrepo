﻿using AdminCallCenter.Class;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace AdminCallCenter.Controllers
{
    public class ClientsFilterController : Controller
    {
        public static string dsnSub = System.Configuration.ConfigurationManager.AppSettings["dsnSub"];

        //***DataTables****
        public static int intFirstSort = 0;
        public static bool isActiveLink;
        public static bool firstSort = false;
        public static string IdPost;
        public static List<DataModel> listData = new List<DataModel>();
        public static List<DataModel> listHeader = new List<DataModel>();
        public static String jsonRegisters = null;
        public static String jsonHeader = null;
        public static List<string> listHidden = new List<string>();
        public static String hiddenJson = null;
        public static List<string> listIDRegister = new List<string>();
        public static String IDRegisterJson = null;
        public static List<string> listLink = new List<string>();
        public static String LinkJson = null;
        //***DataTables****

        // GET: ClientsFilter
        public ActionResult ClientsFilter()
        {
            return View();
        }

        public JsonResult getClientList()
        {
            Response resp = new Response();
            listData = new List<DataModel>();
            listHeader = new List<DataModel>();

            resp = DataAccess.getClients(ref listHeader, ref listData, dsnSub);

            isActiveLink = true;

            listHidden = new List<string>();
            listIDRegister = new List<string>();
            listLink = new List<string>();

            listHidden.Add("ID");
            listIDRegister.Add("ID");
            listLink.Add("ID");

            try
            {
                DataSetToJSON(listHeader, listData, ref jsonHeader, ref jsonRegisters, isActiveLink, listLink, ref LinkJson, listIDRegister, ref IDRegisterJson, listHidden, ref hiddenJson);

                string columns = loadColumns(listHeader);

                return Json(new
                {
                    obj = resp,
                    ObjGrid = jsonHeader,
                    ObjData = jsonRegisters,
                    listRegister = IDRegisterJson,
                    listLink = LinkJson,
                    listHidden = hiddenJson,
                    columns = columns
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new
                {
                    obj = resp,
                    ObjGrid = "",
                    ObjData = "",
                    listRegister = "",
                    listLink = "",
                    listHidden = "",
                    columns = ""
                }, JsonRequestBehavior.AllowGet);
                throw;
            }

            //resp = DataAccess.getAdminSMS(ref listHeader, ref listData, 2, dsnSub);
            //return Json(new { obj = mData, resp = resp }, JsonRequestBehavior.AllowGet);
        }

        public static string loadColumns(List<DataModel> mListHeader)
        {
            try
            {
                StringBuilder sbColumns = new StringBuilder();

                //sbColumns.Append("[");

                sbColumns.Append("[{");
                sbColumns.Append("\"targets\": \"-1\",");
                sbColumns.Append("\"data\": \"null\",");
                sbColumns.Append("\"sWidth\": \"25px\",");
                sbColumns.Append("\"className\": \"dt-center\",");
                sbColumns.Append("\"className\": \"dt-center column0Vis\",");
                sbColumns.Append("\"sortable\": false,");
                sbColumns.Append("\"orderable\": false,");
                if (isActiveLink)
                {
                    sbColumns.Append("\"defaultContent\": \"<a href='#'><i class='fa fa-search fa-1x'></i></a>\"");
                }
                else
                {
                    sbColumns.Append("\"defaultContent\": \"\"");
                }
                sbColumns.Append("},");

                for (int index = 0; index <= mListHeader[0].listProperties.Count() - 1; index++)
                {
                    sbColumns.Append("{");
                    sbColumns.Append("\"aTargets\": [" + index + "],");
                    sbColumns.Append("\"sClass\": \"Visual\", ");

                    //*****Columna del comentario del post para éste caso ya que es un texto demasiado largo
                    if (index == 3)
                    {
                        //sbColumns.Append("\"render\": \"var mDiv='<div class=\\\"text-wrap width-200\\\">' + data + '</div>';return mDiv\", ");

                        //sbColumns.Append("\"render\": \"var mDiv='<div class=\\\"checkbox checkbox-success\\\"><input id=\\\"chkActive\\\" class=\\\"styled\\\" type=\\\"checkbox\\\"' + data + '></div>';return mDiv\", ");
                    }
                    //*****Modificación para solo éste informe

                    sbColumns.Append("\"mData\": \"" + index + "\"");
                    if (index != mListHeader[0].listProperties.Count() - 1)
                    {
                        sbColumns.Append("},");
                    }
                    else
                    {
                        sbColumns.Append("}");
                    }
                }

                //sbColumns.Append("}")
                sbColumns.Append("]");
                //columns.Value = sbColumns.ToString()
                return sbColumns.ToString();
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static void DataSetToJSON(List<DataModel> listHeader, List<DataModel> listData,
            ref String jsonHeader, ref String jsonRegisters, bool isActiveLink, List<string> listLink, ref String linkJson,
            List<string> listIDRegister, ref String idRegisterJson, List<string> listHidden, ref String hiddenJson)
        {

            //convert the filled DT into JSON
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            string outputJson = string.Empty;
            dynamic sb = new StringBuilder();

            serializer.MaxJsonLength = Int32.MaxValue;
            int indexFile = 0;
            int indexColumn = 0;
            string mRegister = null;

            List<Dictionary<string, object>> rowsRegisters = new List<Dictionary<string, object>>();
            List<Dictionary<string, object>> rowsHeaders = new List<Dictionary<string, object>>();
            Dictionary<string, object> row = default(Dictionary<string, object>);
            Dictionary<string, object> rowHeader = default(Dictionary<string, object>);
            List<Dictionary<string, object>> rowsHidden = new List<Dictionary<string, object>>();
            Dictionary<string, object> rowHidden = default(Dictionary<string, object>);

            List<Dictionary<string, object>> rowsRegister = new List<Dictionary<string, object>>();
            Dictionary<string, object> rowRegister = default(Dictionary<string, object>);

            List<Dictionary<string, object>> rowsLink = new List<Dictionary<string, object>>();
            Dictionary<string, object> rowLink = default(Dictionary<string, object>);

            rowHeader = new Dictionary<string, object>();

            if (listData.Count > 0)
            {
                //For Each col As DataColumn In ds.Tables(0).Columns
                for (indexColumn = 0; indexColumn <= listData[0].listProperties.Count() - 1; indexColumn++)
                {
                    mRegister = listHeader[0].listProperties[indexColumn];
                    //rowHeader.Add(mRegister, mRegister)

                    foreach (var item in listHidden)
                    {
                        if (mRegister == item)
                        {
                            rowHidden = new Dictionary<string, object>();
                            rowHidden.Add("HIDDEN", indexColumn);
                            rowsHidden.Add(rowHidden);
                        }
                    }


                    if (isActiveLink)
                    {
                        foreach (var item in listIDRegister)
                        {
                            if (mRegister == item)
                            {
                                rowRegister = new Dictionary<string, object>();
                                rowRegister.Add("IDREGISTER", indexColumn);
                                rowsRegister.Add(rowRegister);
                            }
                        }

                        foreach (var item in listLink)
                        {
                            if (mRegister == item)
                            {
                                rowLink = new Dictionary<string, object>();
                                rowLink.Add("LINK", indexColumn);
                                rowsLink.Add(rowLink);
                            }
                        }
                    }
                }
            }

            for (indexColumn = 0; indexColumn <= listHeader[0].listProperties.Count() - 1; indexColumn++)
            {
                mRegister = listHeader[0].listProperties[indexColumn];
                rowHeader.Add(mRegister, mRegister);
            }

            rowsHeaders.Add(rowHeader);
            jsonHeader = JsonConvert.SerializeObject(rowsHeaders);

            //For Each dr As DataRow In ds.Tables(0).Rows
            for (indexFile = 0; indexFile <= listData.Count - 1; indexFile++)
            {
                row = new Dictionary<string, object>();

                for (indexColumn = 0; indexColumn <= listData[indexFile].listProperties.Count() - 1; indexColumn++)
                {
                    row.Add(listHeader[0].listProperties[indexColumn], listData[indexFile].listProperties[indexColumn]);
                }

                rowsRegisters.Add(row);
            }

            jsonRegisters = JsonConvert.SerializeObject(rowsRegisters);
            hiddenJson = JsonConvert.SerializeObject(rowsHidden);
            idRegisterJson = JsonConvert.SerializeObject(rowsRegister);
            linkJson = JsonConvert.SerializeObject(rowsLink);

        }


        public JsonResult fileExport()
        {

            if ((jsonRegisters != null) & (jsonRegisters != string.Empty))
            {
                return Json(new
                {
                    objJson = jsonRegisters,
                }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json(new
                {
                    objJson = "",
                }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}