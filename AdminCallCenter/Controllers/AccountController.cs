﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System.Web.Security;

namespace AdminCallCenter.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        [AllowAnonymous]
        public ActionResult LogOn(string returnUrl)
        {
            if (Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult LogOn(string username, string password, string returnUrl)
        {
            try
            {
                Security.Users userSecurity = new Security.Users();
                string loginResult = "";
                if (userSecurity.AuthenticateUser(username, password, out loginResult))
                {
                    //Cambio Jose login

                    //if (returnUrl != string.Empty)
                    //{
                    //    return RedirectToLocal(returnUrl);
                    //}
                    //else
                    //{
                    return RedirectToAction("Index", "Home");
                    //}
                }
                else
                {
                    ViewBag.ErrorMessage = loginResult;
                }
                ViewBag.ReturnUrl = returnUrl;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = string.Format("Excepción al incio de sesión: {0}.", ex.Message);
            }
            return View();
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            return RedirectToAction("LogOn", "Account");
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }
    }
}