﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdminCallCenter.DataBase;
using AdminCallCenter.Class;

namespace AdminCallCenter.Controllers
{
    public class ClientsController : Controller
    {
        private AdminCallCenterEntities db = new AdminCallCenterEntities();

        // GET: Clients
        public ActionResult Index()
        {
            var clients = db.Clients.Include(c => c.City1).Include(c => c.DocumentType1);
          
            DataResult mData = new DataResult(); 

            decimal average = 0;

            int age = 0;
            int countAge20 = 0;
            int countAge50 = 0;

            foreach (var item in clients)
            {
                age = (DateTime.Today.AddTicks(-item.Birthdate.Ticks).Year - 1);
                average += age;

                if (age <= 20)
                {
                    countAge20++;
                }

                if (age > 50)
                {
                    countAge50++;
                }
            }         

            mData.AverageAge = average;
            mData.CountAge20 = countAge20;
            mData.CountAge50 = countAge50;

            mData.clients = clients.ToList();

            return View(clients.ToList());
            //return View(mData);
        }

        // GET: Clients/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Client client = db.Clients.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            return View(client);
        }

        // GET: Clients/Create
        public ActionResult Create()
        {
            ViewBag.City = new SelectList(db.Cities, "CityCode", "CityName");
            ViewBag.DocumentType = new SelectList(db.DocumentTypes, "Id_DocumentType", "Description");
            return View();
        }

        // POST: Clients/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id_Client,DocumentType,Document,Name,MobilePhone,City,Birthdate")] Client client)
        {
            if (ModelState.IsValid)
            {
                db.Clients.Add(client);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.City = new SelectList(db.Cities, "CityCode", "CityName", client.City);
            ViewBag.DocumentType = new SelectList(db.DocumentTypes, "Id_DocumentType", "Description", client.DocumentType);
            return View(client);
        }

        // GET: Clients/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Client client = db.Clients.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            ViewBag.City = new SelectList(db.Cities, "CityCode", "CityName", client.City);
            ViewBag.DocumentType = new SelectList(db.DocumentTypes, "Id_DocumentType", "Description", client.DocumentType);

            return View(client);
        }

        // POST: Clients/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id_Client,DocumentType,Document,Name,MobilePhone,City,Birthdate")] Client client)
        {
            if (ModelState.IsValid)
            {
                db.Entry(client).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.City = new SelectList(db.Cities, "CityCode", "CityName", client.City);
            ViewBag.DocumentType = new SelectList(db.DocumentTypes, "Id_DocumentType", "Description", client.DocumentType);
            return View(client);
        }

        // GET: Clients/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Client client = db.Clients.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            return View(client);
        }

        // POST: Clients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Client client = db.Clients.Find(id);
            db.Clients.Remove(client);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
